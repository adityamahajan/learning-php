<?php
    class mainGame
    {
        public $activePlayer;
        public $board = array(
            [0,0,0],
            [0,0,0],
            [0,0,0]
        );
        public function availablePos($path)
        {
            $poss = array();
            for($x=0;$x<3;++$x)
            {
                for($y=0;$y<3;++$y)
                {
                    if($path[$x][$y] == NULL)
                        array_push($poss, array([$x]=>[$y]));
                }
            }
            return $poss;     
        }
    }