<?php

class tictactoe extends game
{
	var $player = "X";			
	var $board = array();		
	var $totalMoves = 0;		
	function tictactoe()
	{
		game::start();
        $this->newBoard();
	}
    function newBoard() {
    
		$this->board = array();
        
        for ($x = 0; $x <= 8; $x++)
            {
                $this->board[$x] = null;
            }
    }
    function newGame()
	{
		$this->start();
		$this->player = "X";
		$this->totalMoves = 0;
        $this->newBoard();
	}
	function playGame($gamedata)
	{
		if (!$this->isOver() && isset($gamedata['move'])) {
			$this->move($gamedata);
        }
            
        if (isset($gamedata['newgame'])) {
			$this->newGame();
        }
				
		$this->displayGame();
	}
	function displayGame()
	{
		if (!$this->isOver())
		{
			echo "<div id=\"board\">";
			
			for ($x = 0; $x < 9; $x++)
			{
					echo '<div class="board_cell">';
					
                    if ($this->board[$x])
                        echo $this->board[$x];
					else
					{
						echo "<select name=\"{$x}\">
								<option value=\"\"></option>
								<option value=\"{$this->player}\">{$this->player}</option>
							</select>";
					}
					
					echo "</div>";
				if($x==2 || $x==5 || $x==8)
				 echo '<div class="break"></div>';
			}
			
			echo "
				<p align=\"center\">
					<input type=\"submit\" name=\"move\" value=\"Take Turn\" /><br/>
					<b>It's player {$this->player}'s turn.</b></p>
			</div>";
		}
		else
		{
			
			if ($this->isOver() != "Tie")
				echo "Player " . $this->isOver() . ",has won the game!";
			else if ($this->isOver() == "Tie")
				echo "Game tied";

			session_destroy(); 
				
			echo "<p align=\"center\"><input type=\"submit\" name=\"newgame\" value=\"New Game\" /></p>";
		}
	}
	function move($gamedata)
	{			

		if ($this->isOver())
			return;
		foreach ($gamedata as $key => $value)
		{
			if ($value == $this->player)
			{	
				$this->board[$key] = $this->player;

				if ($this->player == "X")
					$this->player = "O";
				else
					$this->player = "X";
					
				$this->totalMoves++;
				if(!$this->isOver())
				{
					echo "<div id = \"moves\">";
					printf("Moves left : %s",9-$this->totalMoves);
					echo "</div>";
				}	
				
			}
		}
	
		if ($this->isOver())
			return;
	}
	function isOver()
	{
		if ($this->board[0] && $this->board[0] == $this->board[4] && $this->board[4] == $this->board[8])
			return $this->board[0];
			
		if ($this->board[2] && $this->board[2] == $this->board[4] && $this->board[4] == $this->board[6])
			return $this->board[2];
		
		for($x=0;$x<7;$x+=3)
		{
			if ($this->board[$x] && $this->board[$x] == $this->board[$x+1] && $this->board[$x+1] == $this->board[$x+2])
				return $this->board[$x];	
		}
		for($x=0;$x<3;$x++)
		{
			if ($this->board[$x] && $this->board[$x] == $this->board[$x+3] && $this->board[$x+3] == $this->board[$x+6])
				return $this->board[$x];	
		}

		if ($this->totalMoves >= 9)
			return "Tie";
	}
}
