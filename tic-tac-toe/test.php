<?php
    require('gameAI.php');
    require('gameStatus.php');
    require('PlayNextPath.php');
    require('PathChangedSubject.php');
    require('gamePath.php');
    require('mainGame.php');
    require('position.php');
   
    $game = new mainGame();
    $pos = new position(0,0);
    $path = new gamePath();
    $ai = new gameAI();
    $status = new gameStatus();
    $sub = new PathChangedSubject();
    $pnp = new PlayNextPath();
    $ai->initStart('X');
    
