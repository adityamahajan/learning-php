<?php
    
    class PathChangedSubject 
    {
        public $subscribers = array();
        public function subscribe($subscriber) 
        {
            $this->subscribers[] = $subscriber;
        }
        public function trigger($path) 
        {
            foreach ($subscribers as $subscriber) 
            {
                $subscriber->handle($path);
            }
        }
    }