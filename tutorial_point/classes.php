<?php
   echo "hello";
   echo "<br>";
   class Books
   {
       var $price;
       var $title;
       function __construct($par1,$par2)
       {
           $this->price = $par2;
           $this->title = $par1;
       }
       function setPrice($par)
       {
           $this->price=$par;
       }
       function getPrice()
       {
           echo $this->price."<br>";
       }
       function setTitle($par)
       {
           $this->title=$par;
       }
       function getTitle()
       {
           echo $this->title."<br>";
       }
   }
   class Novel extends Books
   {
       var $publisher;
       function setPublisher($var)
       {
           $this->publisher = $var;
       }
       function getPublisher()
       {
           echo $this->publisher."<br>";
       }
   }
   $physics = new Books("Physics for High School",10);
   $maths = new Books("Advanced Chemistry",15);
   $chemistry = new Books("Algebra",7);
  /* $physics->setTitle("Physics for High School");
   $chemistry->setTitle( "Advanced Chemistry" );
   $maths->setTitle( "Algebra" );
   $physics->setPrice( 10 );
   $chemistry->setPrice( 15 );
   $maths->setPrice( 7 );*/
   $physics->getTitle();
   $chemistry->getTitle();
   $maths->getTitle();
   $physics->getPrice();
   $chemistry->getPrice();
   $maths->getPrice();
   class MyClass {
    const CONST_VALUE = 'A constant value';
}

$classname = 'MyClass';
echo $classname::CONST_VALUE; // As of PHP 5.3.0
echo "<br>";
echo MyClass::CONST_VALUE; 